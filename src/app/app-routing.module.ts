import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { LoginComponent } from './login/login.component';
import { RegisterComponent } from './register/register.component';
import { NavBarComponent } from './nav-bar/nav-bar.component';
import { HomeComponent } from './home/home.component';
import { AboutComponent } from './about/about.component';
import { ContactsComponent } from './contacts/contacts.component';
import { ServicesComponent } from './services/services.component';
import {GalleryComponent} from './gallery/gallery.component';
import { BlogsComponent } from './blogs/blogs.component';

const routes: Routes = [
  {
    path:'login',
    component:LoginComponent
  },
  {
    path:'register',
    component:RegisterComponent
  },
  {
    path:'',
    redirectTo:'/login',
    pathMatch:'full'
  },
  
  {
    path:'',
    component:NavBarComponent,
    children:[{
      path:'home',
      component:HomeComponent,
    
    },
    {
      path:'about',
      component:AboutComponent
    },
    {
      path:'contacts',
      component:ContactsComponent
    },
   {
    path:'services',
    component:ServicesComponent
   },
   {
    path:'gallery',
    component:GalleryComponent
   },
   {
    path:'blogs',
    component:BlogsComponent
   },


  ]
    
  }

];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
