import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MatToolbarModule,MatIconModule,MatSidenavModule,MatListModule,MatButtonModule} from '@angular/material';
import { LoginComponent } from './login/login.component';
import { RegisterComponent } from './register/register.component';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { NavBarComponent } from './nav-bar/nav-bar.component';
import { NavBarModule } from './nav-bar/nav-bar.module';
import { HomeComponent } from './home/home.component';
import {GoogleApiModule,GoogleApiService,GoogleAuthService,NgGapiClientConfig,NG_GAPI_CONFIG,GoogleApiConfig} from "ng-gapi";
import { UserService } from './user.service';
import {NgbModule} from '@ng-bootstrap/ng-bootstrap';
import { AboutComponent } from './about/about.component';
import { ContactsComponent } from './contacts/contacts.component';
import { ServicesComponent } from './services/services.component';
import { GalleryComponent } from './gallery/gallery.component';
import { BlogsComponent } from './blogs/blogs.component';


let gapiClientConfig: NgGapiClientConfig = {
  client_id: "305896939646-sbhd2daeov1gbkuj23fcqtp4uii34ftj.apps.googleusercontent.com",
  discoveryDocs: ["https://analyticsreporting.googleapis.com/$discovery/rest?version=v4"],
  scope: [
      "https://www.googleapis.com/auth/analytics.readonly",
      "https://www.googleapis.com/auth/analytics"
  ].join(" ")
};
@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    RegisterComponent,
    NavBarComponent,
    HomeComponent,
    AboutComponent,
    ContactsComponent,
    ServicesComponent,
    GalleryComponent,
    BlogsComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    MatToolbarModule,
    MatIconModule,
    MatSidenavModule,
    MatListModule,
    MatButtonModule,
    HttpClientModule,
    MatListModule,
    ReactiveFormsModule,
    NavBarModule,
    FormsModule,
    NgbModule,
    
    
    GoogleApiModule.forRoot({
      provide: NG_GAPI_CONFIG,
      useValue: gapiClientConfig
    }),
  
  ],
  providers: [UserService],
  bootstrap: [AppComponent]
})
export class AppModule { }
