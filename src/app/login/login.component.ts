import { Component, OnInit, AfterViewInit, NgZone } from '@angular/core';
import { FormGroup } from '@angular/forms';
import { GoogleApiService, GoogleAuthService } from 'ng-gapi';
import { UserService } from '../user.service';
import { Router } from '@angular/router';
declare const gapi: any;

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})

export class LoginComponent implements OnInit ,AfterViewInit {
	auth2: any;
	public  gapi: any;

 constructor(private gapiService:GoogleApiService,
	private userService:UserService,private ngZone:NgZone,
	private googleAuthService:GoogleAuthService,
	private router:Router)
 {
	this.gapiService.onLoad().subscribe((response)=> {
		// Here we can use gapi
		console.log(response);
		
		
	 });

 }

 ngOnInit(){
  const signUpButton = document.getElementById('signUp');
	const signInButton = document.getElementById('signIn');
	const container = document.getElementById('container');
	
	signUpButton.addEventListener('click', () => {
		container.classList.add("right-panel-active");
	});
	
	signInButton.addEventListener('click', () => {
		container.classList.remove("right-panel-active");
	});

	

 }

 ngAfterViewInit(){
	this.getGooglUser();
 }
	private getGooglUser() {
		gapi.load!('auth2', () => {
			this.auth2 = gapi.auth2.init({
				client_id: '305896939646-sbhd2daeov1gbkuj23fcqtp4uii34ftj.apps.googleusercontent.com',
				cookiepolicy: 'single_host_origin',
				redirect_uri: 'http://localhost:4200',
				scope: 'profile email https://www.googleapis.com/auth/calendar.events'
			});
			
			this.attachSignin(document.getElementById('googleBtn'));
		});
	}

 attachSignin(element){
	this.auth2.attachClickHandler(element, {},
		(googleUser) => {
			var accessToken = googleUser.getAuthResponse().access_token;
			console.log(googleUser);
			this.ngZone.run(() => {
			this.getUserDetails(googleUser);
	});

		})

 }

 private getUserDetails(googleUser: any) {
	
		// debugger
		// const name = googleUser.getBasicProfile().getName();
		const email = googleUser.getBasicProfile().getEmail();
		const userprofile = googleUser.getBasicProfile().getImageUrl();
		if (email) {
			localStorage.userProfile = userprofile;
			localStorage.name = name;
			this.router.navigate(['/home']);
		}
		console.log(googleUser);
}

signInWithGoogle(){
	this.userService.getToken();
	
}
 

  

}
