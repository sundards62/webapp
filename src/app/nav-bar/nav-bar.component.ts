import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-nav-bar',
  templateUrl: './nav-bar.component.html',
  styleUrls: ['./nav-bar.component.css']
})
export class NavBarComponent implements OnInit {
 
  userName:'';
  userProfile:'';
  constructor(private router:Router) { }

  ngOnInit() {
    this.userName = localStorage.name;
    this.userProfile = localStorage.userProfile;
  }
signOut(){
this.router.navigate(['/login']);
localStorage.clear();
}
}

